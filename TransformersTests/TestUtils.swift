//
// Created by Zapko on 2019-09-05.
// Copyright (c) 2019 Zababako Studio Inc. All rights reserved.
//

import XCTest
@testable import Transformers


extension XCTestCase {

    func testAsyncCode(
        _ comment: String,
        timeout:   TimeInterval = 5,
        file:      StaticString = #file,
        line:      UInt = #line,
        test:      (@escaping () -> Void) throws -> Void
    ) -> Void {

        let callEnds = expectation(description: comment)
        do {
            try test { callEnds.fulfill() }
        } catch {
            XCTFail("Test threw an error: '\(error)'", file: file, line: line)
            callEnds.fulfill()
        }

        waitForExpectations(timeout: timeout)
    }
}


// MARK: - Transformers catalogue

let seaspray____A_3__8_6_6__6_10_6_7 = try! Transformer(from: "Seaspray,    A, 3,  8, 6, 6,  6, 10, 6, 7")
let ramhorn_____A_8__4_3_9__5_9__3_4 = try! Transformer(from: "Ramhorn,     A, 8,  4, 3, 9,  5, 9,  3, 4")
let searchlight_A_3__7_5_8__6_4__1_9 = try! Transformer(from: "Searchlight, A, 3,  7, 5, 8,  6, 4,  1, 9")
let abominus____D_10_1_3_10_5_10_8_4 = try! Transformer(from: "Abominus,    D, 10, 1, 3, 10, 5, 10, 8, 4")
let battletrap__D_7__3_6_8__6_6__7_7 = try! Transformer(from: "Battletrap,  D, 7,  3, 6, 8,  6, 6,  7, 7")
let barrage_____D_3__8_3_7__5_10_9_8 = try! Transformer(from: "Barrage,     D, 3,  8, 3, 7,  5, 10, 9, 8")

