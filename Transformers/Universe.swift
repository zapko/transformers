//
//  Universe.swift
//  transformers
//
//  Created by Konstantin Zabelin on 2019-09-04.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import Foundation
import PromiseKit


typealias Spark = String

enum Team: Equatable, Codable {
    case autobots
    case decepticons
}

struct Transformer: Codable, Hashable {

    typealias ID = String

    let id:   ID! // id will be nil for Transformers that hasn't been posted to the universe yet
    var name: String

    var team: Team
    var rank: Grade

    var courage: Grade
    var skill:   Grade

    var strength:     Grade
    var intelligence: Grade
    var speed:        Grade
    var endurance:    Grade
    var firepower:    Grade

    var overallRating: Int {
        return [strength, intelligence, speed, endurance, firepower]
            .map { $0.value }
            .reduce(0, +)
    }
}

/// Wrapper for Integer that needs to be in range from 1 to 10
struct Grade: Equatable, ExpressibleByIntegerLiteral, Codable, Hashable, Comparable {
    var value: Int
}

struct Battle {

    enum Pair: Equatable {
        case onlyAutobot(Transformer)
        case onlyDecepticon(Transformer)
        case pair(A: Transformer, D: Transformer)
    }

    struct Result: Equatable {
        let survived: [Transformer]
        let perished: [Transformer]
    }

    enum Error: Swift.Error, Equatable {
        case endOfTheWorld
    }
}

struct War {

    struct Result: Equatable {
        let winner:   Team?
        let survived: [Transformer]
        let perished: [Transformer]
    }
}

extension War {

    static func formPairs(from participants: [Transformer]) -> [Battle.Pair] {

        let sorter: (Transformer, Transformer) -> Bool = {

            if $0.rank > $1.rank { return true  }
            if $0.rank < $1.rank { return false }

            if $0.id < $1.id { return true  }
            if $0.id > $1.id { return false }

            return true
        }

        let autobots    = participants.filter { $0.team == .autobots    }.sorted(by: sorter)
        let decepticons = participants.filter { $0.team == .decepticons }.sorted(by: sorter)

        return (0..<max(autobots.count, decepticons.count)).map {

            switch $0 {
            case let i where i < autobots.count && i < decepticons.count:
                return .pair(A: autobots[i], D: decepticons[i])

            case let i where i < autobots.count:
                return .onlyAutobot(autobots[i])

            case let i where i < decepticons.count:
                return .onlyDecepticon(decepticons[i])

            default:
                fatalError("Impossible case")
            }
        }
    }

    static func fight(_ battlePair: Battle.Pair) throws -> Battle.Result {

        let survived: [Transformer]
        let perished: [Transformer]

        switch battlePair {
        case .onlyAutobot(let autobot):
            survived = [autobot]
            perished = []

        case .onlyDecepticon(let decepticon):
            survived = [decepticon]
            perished = []

        case let .pair(autobot, decepticon):

            let formalizer: (Int) -> ([Transformer], [Transformer]) = {
                if $0 > 0 {
                    return ([autobot], [decepticon]) // Autobot wins
                } else if $0 < 0{
                    return ([decepticon], [autobot]) // Decepticon wins
                } else {
                    return ([], [autobot, decepticon]) // Tie
                }
            }

            if wunderWaffe.contains(autobot.name) && wunderWaffe.contains(decepticon.name) {
                throw Battle.Error.endOfTheWorld
            }

            if wunderWaffe.contains(autobot.name) {

                (survived, perished) = formalizer(1)
                break
            }

            if wunderWaffe.contains(decepticon.name) {

                (survived, perished) = formalizer(-1)
                break
            }

            let courageDiff  = autobot.courage.value  - decepticon.courage.value
            let strengthDiff = autobot.strength.value - decepticon.strength.value

            let skillDiff = autobot.skill.value - decepticon.skill.value

            let ratingDiff = autobot.overallRating - decepticon.overallRating

            if abs(courageDiff)  >= 4 &&
               abs(strengthDiff) >= 3 &&
               abs(courageDiff + strengthDiff) >= 7 {

                (survived, perished) = formalizer(courageDiff)

            } else if abs(skillDiff) >= 3 {

                (survived, perished) = formalizer(skillDiff)

            } else {

                (survived, perished) = formalizer(ratingDiff)

            }
        }

        return Battle.Result(
            survived: survived,
            perished: perished
        )
    }

    static func startWar(_ participants: [Transformer]) -> War.Result {

        do {
            let pairs = formPairs(from: participants)
            let battleResults = try pairs.map(War.fight)

            let perished = battleResults.flatMap { $0.perished }
            let survived = battleResults.flatMap { $0.survived }

            let killedAutobots    = perished.filter { $0.team == .autobots    }.count
            let killedDecepticons = perished.filter { $0.team == .decepticons }.count

            let winner: Team?

            if killedDecepticons > killedAutobots  {
                winner = .autobots
            } else if killedAutobots > killedDecepticons {
                winner = .decepticons
            } else {
                winner = nil
            }

            return War.Result(
                winner:   winner,
                survived: survived,
                perished: perished
            )

        } catch {
            return War.Result(
                winner:   nil,
                survived: [],
                perished: participants
            )
        }
    }
}


// MARK: - Implementation details

extension Transformer {

    init() {
        self.init(
            id:           nil,
            name:         "",
            team:         .autobots,
            rank:         5,
            courage:      5,
            skill:        5,
            strength:     5,
            intelligence: 5,
            speed:        5,
            endurance:    5,
            firepower:    5
        )
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(name)
        hasher.combine(team)
        hasher.combine(rank)
        hasher.combine(courage)
        hasher.combine(skill)
        hasher.combine(strength)
        hasher.combine(speed)
        hasher.combine(endurance)
        hasher.combine(firepower)
        hasher.combine(intelligence)
    }

    static let characteristicsPaths: [WritableKeyPath<Transformer, Grade>] = [
        \.strength,
        \.intelligence,
        \.speed,
        \.endurance,
        \.rank,
        \.courage,
        \.firepower,
        \.skill
    ]
}

extension Grade {


    // MARK: - ExpressibleByIntegerLiteral

    init(integerLiteral value: Int) {
        switch value {
        case let x where x > 10: self.value = 10
        case let x where x < 1:  self.value = 1
        default:                 self.value = value
        }
    }


    // MARK: - Hashable

    func hash(into hasher: inout Hasher) {
        hasher.combine(value)
    }


    // MARK: - Comparable
    
    static func < (lhs: Grade, rhs: Grade) -> Bool {
        return lhs.value < rhs.value
    }
}

private let wunderWaffe: Set<String> = Set(["Optimus Prime", "Predaking"])


// Some samples to play around
extension Transformer {
    static let seaspray____A_3__8_6_6__6_10_6_7 = try! Transformer(from: "Seaspray,    A, 3,  8, 6, 6,  6, 10, 6, 7")
    static let ramhorn_____A_8__4_3_9__5_9__3_4 = try! Transformer(from: "Ramhorn,     A, 8,  4, 3, 9,  5, 9,  3, 4")
    static let searchlight_A_3__7_5_8__6_4__1_9 = try! Transformer(from: "Searchlight, A, 3,  7, 5, 8,  6, 4,  1, 9")
    static let abominus____D_10_1_3_10_5_10_8_4 = try! Transformer(from: "Abominus,    D, 10, 1, 3, 10, 5, 10, 8, 4")
    static let battletrap__D_7__3_6_8__6_6__7_7 = try! Transformer(from: "Battletrap,  D, 7,  3, 6, 8,  6, 6,  7, 7")
    static let barrage_____D_3__8_3_7__5_10_9_8 = try! Transformer(from: "Barrage,     D, 3,  8, 3, 7,  5, 10, 9, 8")
}
