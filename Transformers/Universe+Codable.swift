//
//  Universe+Codable.swift
//  transformers
//
//  Created by Konstantin Zabelin on 2019-09-04.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import Foundation


enum CodingError: Error, Equatable {
    case unexpectedTeam(String)
    case gradeOutOfRange(Int)
    case wrongNumberOfValues(Int)
    case valueFormatViolation(String)
    case unreadableAllSparkCache(String)
}

extension Transformer {

    init(from string: String) throws {

        let values = string
            .components(separatedBy: ",")
            .map { $0.trimmingCharacters(in: .whitespacesAndNewlines) }
            .filter { !$0.isEmpty }

        guard values.count == 10 else {
            throw CodingError.wrongNumberOfValues(values.count)
        }

        self.id   = values[0]
        self.name = values[0]

        switch values[1] {
        case "A": self.team = .autobots
        case "D": self.team = .decepticons
        default: throw CodingError.valueFormatViolation(values[1])
        }

        let parseAtIndex: (Int) throws -> Grade = {
            index in

            guard let value = Int(values[index]) else {
                throw CodingError.valueFormatViolation(values[index])
            }

            return Grade(value: value)
        }

        self.strength     = try parseAtIndex(2)
        self.intelligence = try parseAtIndex(3)
        self.speed        = try parseAtIndex(4)
        self.endurance    = try parseAtIndex(5)
        self.rank         = try parseAtIndex(6)
        self.courage      = try parseAtIndex(7)
        self.firepower    = try parseAtIndex(8)
        self.skill        = try parseAtIndex(9)
    }
}

extension Grade {

    init(from decoder: Decoder) throws {

        let container  = try decoder.singleValueContainer()
        let grade = try container.decode(Int.self)

        if grade < 1  { throw CodingError.gradeOutOfRange(grade) }
        if grade > 10 { throw CodingError.gradeOutOfRange(grade) }

        self.value = grade
    }

    func encode(to encoder: Encoder) throws {

        var container = encoder.singleValueContainer()
        try container.encode(self.value)
    }
}

extension Team {

    init(from decoder: Decoder) throws {

        let container  = try decoder.singleValueContainer()
        let teamSymbol = try container.decode(String.self)

        switch teamSymbol {
        case "A": self = .autobots
        case "D": self = .decepticons
        default: throw CodingError.unexpectedTeam(teamSymbol)
        }
    }

    func encode(to encoder: Encoder) throws {

        let teamSymbol: String
        switch self {
        case .autobots:    teamSymbol = "A"
        case .decepticons: teamSymbol = "D"
        }

        var container = encoder.singleValueContainer()
        try container.encode(teamSymbol)
    }
}
