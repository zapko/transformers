//
//  Universe+Codable_Spec.swift
//  transformers
//
//  Created by Konstantin Zabelin on 2019-09-04.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import XCTest
import Foundation
@testable import Transformers


class Universe_Codable_spec: XCTestCase {

    func test_Transformer_decodes_from_comma_separated_list_of_name_team_strength_intelligence_speed_endurance_rank_courage_firepower_skill() throws {

        let stringEncodedTransformer = "John, A, 1, 2, 3, 4, 5, 6, 7, 8"

        let transformer = try Transformer(from: stringEncodedTransformer)

        XCTAssertEqual(transformer.name,         "John")
        XCTAssertEqual(transformer.team,         Team.autobots)
        XCTAssertEqual(transformer.strength,     1 as Grade)
        XCTAssertEqual(transformer.intelligence, 2 as Grade)
        XCTAssertEqual(transformer.speed,        3 as Grade)
        XCTAssertEqual(transformer.endurance,    4 as Grade)
        XCTAssertEqual(transformer.rank,         5 as Grade)
        XCTAssertEqual(transformer.courage,      6 as Grade)
        XCTAssertEqual(transformer.firepower,    7 as Grade)
        XCTAssertEqual(transformer.skill,        8 as Grade)
    }

    func test_Transformer_decodes_Optimus_Prime_from_comma_separated_list() throws {

        let optimus = try Transformer(from: "Optimus Prime, A, 10, 10, 8, 10, 10, 10, 8, 10")

        XCTAssertEqual(optimus.name,         "Optimus Prime")
        XCTAssertEqual(optimus.team,         Team.autobots)
        XCTAssertEqual(optimus.strength,     10 as Grade)
        XCTAssertEqual(optimus.intelligence, 10 as Grade)
        XCTAssertEqual(optimus.speed,        8  as Grade)
        XCTAssertEqual(optimus.endurance,    10 as Grade)
        XCTAssertEqual(optimus.rank,         10 as Grade)
        XCTAssertEqual(optimus.courage,      10 as Grade)
        XCTAssertEqual(optimus.firepower,    8  as Grade)
        XCTAssertEqual(optimus.skill,        10 as Grade)
    }


    func test_Transformer_can_be_decoded_from_json() throws {

        let json = """
                   {
                   "id": "-LLbrUN3dQkeejt9vTZc",
                   "name": "Megatron",
                   "strength": 10,
                   "intelligence": 10,
                   "speed": 4,
                   "endurance": 8,
                   "rank": 10,
                   "courage": 9,
                   "firepower": 10,
                   "skill": 9,
                   "team": "D"
                   }
                   """.data(using: .utf8)!

        let transformer = try JSONDecoder().decode(Transformer.self, from: json)

        XCTAssertEqual(transformer.id,           "-LLbrUN3dQkeejt9vTZc")
        XCTAssertEqual(transformer.name,         "Megatron")
        XCTAssertEqual(transformer.strength,     10 as Grade)
        XCTAssertEqual(transformer.intelligence, 10 as Grade)
        XCTAssertEqual(transformer.speed,        4  as Grade)
        XCTAssertEqual(transformer.endurance,    8  as Grade)
        XCTAssertEqual(transformer.rank,         10 as Grade)
        XCTAssertEqual(transformer.courage,      9  as Grade)
        XCTAssertEqual(transformer.firepower,    10 as Grade)
        XCTAssertEqual(transformer.skill,        9  as Grade)
        XCTAssertEqual(transformer.team,         Team.decepticons)
    }

    func test_Transformer_can_be_encoded_and_decoded_back_with_no_changes() throws {

        let transformerBefore = Transformer(
            id:           "123",
            name:         "Steeljaw",
            team:         .autobots,
            rank:         6,
            courage:      9,
            skill:        9,
            strength:     8,
            intelligence: 8,
            speed:        2,
            endurance:    9,
            firepower:    1
        )

        let encodedTransformer = try JSONEncoder().encode(transformerBefore)
        let transformerAfter   = try JSONDecoder().decode(Transformer.self, from: encodedTransformer)

        XCTAssertEqual(transformerAfter, transformerBefore)
    }

    func test_Transformer_decoding_throws_if_team_value_is_unknown() throws {

        let json = """
                   {
                   "id": "-LLbrUN3dQkeejt9vTZc",
                   "name": "Megatron",
                   "strength": 10,
                   "intelligence": 10,
                   "speed": 4,
                   "endurance": 8,
                   "rank": 10,
                   "courage": 9,
                   "firepower": 10,
                   "skill": 9,
                   "team": "BRB"
                   }
                   """.data(using: .utf8)!

        XCTAssertThrowsError(try JSONDecoder().decode(Transformer.self, from: json)) {
            error in
            XCTAssertEqual(error as? CodingError, CodingError.unexpectedTeam("BRB"))
        }
    }

    func test_Transformer_decoding_throws_if_grade_is_less_than_1() {

        let json = """
                   {
                   "id": "-LLbrUN3dQkeejt9vTZc",
                   "name": "Megatron",
                   "strength": 10,
                   "intelligence": 10,
                   "speed": 0,
                   "endurance": 8,
                   "rank": 10,
                   "courage": 9,
                   "firepower": 10,
                   "skill": 9,
                   "team": "D"
                   }
                   """.data(using: .utf8)!

        XCTAssertThrowsError(try JSONDecoder().decode(Transformer.self, from: json)) {
            error in
            XCTAssertEqual(error as? CodingError, CodingError.gradeOutOfRange(0))
        }
    }

    func test_Transformer_decoding_throws_if_grade_is_more_than_10() {

        let json = """
                   {
                   "id": "-LLbrUN3dQkeejt9vTZc",
                   "name": "Megatron",
                   "strength": 10,
                   "intelligence": 11,
                   "speed": 0,
                   "endurance": 8,
                   "rank": 10,
                   "courage": 9,
                   "firepower": 10,
                   "skill": 9,
                   "team": "D"
                   }
                   """.data(using: .utf8)!

        XCTAssertThrowsError(try JSONDecoder().decode(Transformer.self, from: json)) {
            error in
            XCTAssertEqual(error as? CodingError, CodingError.gradeOutOfRange(11))
        }
    }
}
