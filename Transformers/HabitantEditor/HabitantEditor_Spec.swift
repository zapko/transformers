//
//  HabitantEditor_Spec.swift
//  TransformersTests
//
//  Created by Zapko on 2019-09-08.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import XCTest
@testable import Transformers


class HabitantEditor_Spec: XCTestCase {

    private struct Test {
        let subject: HabitantEditor.Logic

        let viewMock:     ViewMock
        let delegateMock: DelegateMock
    }

    func test_When_cancel_button_is_pressed_Logic_reports_to_delegate() {

        let test = prepareTest(nil)

        test.subject.didPressCancel()

        XCTAssertEqual(test.delegateMock.t_editingCancelledCallCounter, 1)
    }

    func test_When_confirm_button_is_pressed_Logic_reports_current_version_of_transformer_to_delegate() {

        let test = prepareTest(seaspray____A_3__8_6_6__6_10_6_7)

        test.subject.didPressConfirm()

        XCTAssertEqual(test.delegateMock.t_editingConfirmedResults, [seaspray____A_3__8_6_6__6_10_6_7])
    }

    func test_When_team_is_updated_Logic_changes_team_in_current_version_of_transformer_and_sends_it_to_the_view() throws {

        let input          = try Transformer(from: "Seaspray, A, 3, 8, 6, 6, 6, 10, 6, 7")
        let expectedOutput = try Transformer(from: "Seaspray, D, 3, 8, 6, 6, 6, 10, 6, 7")
        
        let test = prepareTest(input)

        test.subject.didUpdateTeam(.decepticons)

        XCTAssertEqual(test.viewMock.state.transformer, expectedOutput)
    }

    func test_When_name_is_updated_Logic_changes_name_in_current_version_of_transformer_and_sends_it_to_the_view() throws {

        let input          = try Transformer(from: "Seaspray,  A, 3, 8, 6, 6, 6, 10, 6, 7")
        var expectedOutput = input; expectedOutput.name = "Earthworm"

        let test = prepareTest(input)

        test.subject.didUpdateName("Earthworm")

        XCTAssertEqual(test.viewMock.state.transformer, expectedOutput)
    }

    func test_When_characteristic_is_updated_Logic_changes_it_in_current_version_of_transformer_and_sends_it_to_the_view() throws {

        let input          = try Transformer(from: "Seaspray, A, 3, 8, 6, 6, 6, 10, 6, 7")
        let expectedOutput = try Transformer(from: "Seaspray, A, 9, 8, 6, 6, 6, 10, 6, 7")

        let test = prepareTest(input)

        test.subject.didUpdateValue(path: \.strength, value: 9)

        XCTAssertEqual(test.viewMock.state.transformer, expectedOutput)
    }

    func test_When_busy_state_is_changed_on_editor_Logic_switches_loading_state() {

        let test = prepareTest(nil)

        test.subject.busy = true
        XCTAssertTrue(test.viewMock.state.loading)

        test.subject.busy = false
        XCTAssertFalse(test.viewMock.state.loading)
    }


    // MARK: - Private Methods

    private func prepareTest(_ transformer: Transformer?) -> Test {

        let viewMock     = ViewMock()
        let delegateMock = DelegateMock()

        let subject = HabitantEditor.Logic(
            view:        viewMock,
            delegate:    delegateMock ,
            transformer: transformer
        )

        return Test(
            subject:      subject,
            viewMock:     viewMock,
            delegateMock: delegateMock
        )
    }

}

private class ViewMock: HabitantEditorView {
    var state: HabitantEditor.ViewState = HabitantEditor.ViewState()
}

private class DelegateMock: HabitantEditorDelegate {

    var t_editingConfirmedResults: [Transformer] = []
    func editingConfirmed(withResult: Transformer) {
        t_editingConfirmedResults.append(withResult)
    }

    var t_editingDeletedTransformers: [Transformer] = []
    func editingDeleted(_ transformer: Transformer) {
        t_editingDeletedTransformers.append(transformer)
    }

    var t_editingCancelledCallCounter = 0
    func editingCancelled() {
        t_editingCancelledCallCounter += 1
    }
}
