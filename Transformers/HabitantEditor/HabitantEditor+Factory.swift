//
//  HabitantEditor+Factory.swift
//  Transformers
//
//  Created by Konstantin Zabelin on 2019-09-06.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import UIKit
import Foundation


extension HabitantEditor {

    struct Factory: MockableModule {

        func make(
            transformer: Transformer?,
            delegate:    HabitantEditorDelegate
        ) -> (UIViewController, HabitantEditorModule) {

            let view = HabitantEditorViewController(nibName: nil, bundle: nil)
            view.title = "Editor"

            let logic = HabitantEditor.Logic(view: view, delegate: delegate, transformer: transformer)
            view.output = logic

            return (UINavigationController(rootViewController: view), logic)
        }


        // MARK: - MockableModule

        func mock(state: String) -> UIViewController {

            let view = HabitantEditorViewController(nibName: nil, bundle: Bundle(for: HabitantEditorViewController.self))
            view.title = "Editor"

            view.state.transformer = (try? Transformer(from: state)) ?? Transformer(
                id: nil,
                name: state,
                team: .autobots,
                rank: 5,
                courage: 5,
                skill: 5,
                strength: 7,
                intelligence: 5,
                speed: 5,
                endurance: 5,
                firepower: 5
            )

            return UINavigationController(rootViewController: view)
        }
    }
}
