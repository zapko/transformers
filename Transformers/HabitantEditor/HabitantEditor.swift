//
//  HabitantEditor.swift
//  Transformers
//
//  Created by Konstantin Zabelin on 2019-09-06.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import Foundation


protocol HabitantEditorDelegate: AnyObject {
    func editingConfirmed(withResult: Transformer)
    func editingDeleted(_ transformer: Transformer)
    func editingCancelled()
}

protocol HabitantEditorModule {
    var busy: Bool { get set }
}

protocol HabitantEditorViewOutput {

    func didPressConfirm()
    func didPressCancel()
    func didPressDelete()

    func didUpdateName(_ name: String)
    func didUpdateTeam(_ team: Team)
    func didUpdateValue(path: WritableKeyPath<Transformer, Grade>, value: Int)
}

struct HabitantEditor {

    struct ViewState: Equatable {
        var loading:     Bool
        var transformer: Transformer

        init () {
            self.loading     = false
            self.transformer = Transformer()
        }
    }

    class Logic: HabitantEditorModule,
                 HabitantEditorViewOutput {


        // MARK: - Private State

        private weak var view: HabitantEditorView?

        private weak var delegate: HabitantEditorDelegate?

        private var transformer: Transformer {
            didSet {
                view?.state.transformer = transformer
            }
        }


        // MARK: - Initialization / Deinitialization

        init(
            view:        HabitantEditorView,
            delegate:    HabitantEditorDelegate,
            transformer: Transformer?
        ) {
            self.view        = view
            self.delegate    = delegate
            self.transformer = transformer ?? Transformer()

            view.state.transformer = self.transformer
        }


        // MARK: - HabitantEditorModule

        var busy: Bool = false {
            didSet {
                view?.state.loading = busy
            }
        }


        // MARK: - HabitantsListViewOutput

        func didPressConfirm() {
            delegate?.editingConfirmed(withResult: transformer)
        }

        func didPressCancel() {
            delegate?.editingCancelled()
        }

        func didPressDelete() {

            guard let transformer = view?.state.transformer else { return }

            delegate?.editingDeleted(transformer)
        }

        func didUpdateName(_ name: String) {
            transformer.name = name
        }

        func didUpdateTeam(_ team: Team) {
            transformer.team = team
        }

        func didUpdateValue(path: WritableKeyPath<Transformer, Grade>, value: Int) {
            transformer[keyPath: path] = Grade(value: value)
        }
    }
}
