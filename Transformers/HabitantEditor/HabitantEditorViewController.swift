//
//  HabitantEditorViewController.swift
//  Transformers
//
//  Created by Konstantin Zabelin on 2019-09-06.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import UIKit


protocol HabitantEditorView: AnyObject {
    var state: HabitantEditor.ViewState { get set }
}

class HabitantEditorViewController: UIViewController, HabitantEditorView, UITextFieldDelegate {


    // MARK: - Private State

    private let loaderItem: UIBarButtonItem = {

        let loader = UIActivityIndicatorView(style: .gray)

        loader.hidesWhenStopped = true
        loader.startAnimating()

        return UIBarButtonItem(customView: loader)
    }()

    private let confirmButton = UIBarButtonItem(
        title: "Confirm",
        style: .plain,
        target: self,
        action: #selector(confirmPressed)
    )

    private let paths = Transformer.characteristicsPaths

    @IBOutlet weak var nameField:            UITextField!
    @IBOutlet weak var teamSegmentedControl: UISegmentedControl!
    @IBOutlet var characteristicsLabels:   [UILabel]!
    @IBOutlet var characteristicsSteppers: [UIStepper]!
    /// Tags in steppers and labels match indices of corresponding properties in Transformer.characteristicsPaths
    @IBOutlet weak var deleteButton: UIButton!

    
    // MARK: - HabitantEditorViewController

    @IBAction func characteristicChanged(sender: UIStepper) {
        output.didUpdateValue(path: paths[sender.tag], value: Int(sender.value))
    }
    
    @IBAction func teamChanged(_ sender: UISegmentedControl) {

        let newTeam: Team
        switch sender.selectedSegmentIndex {
        case 0: newTeam = .autobots
        case 1: newTeam = .decepticons
        default: assertionFailure("Unexpected segment selected"); return
        }

        output.didUpdateTeam(newTeam)
    }
    
    @IBAction func nameDidChange(_ sender: UITextField) {
        output.didUpdateName(sender.text ?? "")
    }

    @IBAction func pressedDelete() {
        output.didPressDelete()
    }
    
    var output: HabitantEditorViewOutput!
    

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: "Cancel",
            style: .plain,
            target: self,
            action: #selector(cancelPressed)
        )

        update(to: state)
    }


    // MARK: - HabitantEditorView

    var state: HabitantEditor.ViewState = HabitantEditor.ViewState() {
        didSet {

            if oldValue == state { return }

            update(to: state)
        }
    }


    // MARK: - UITextFieldDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }


    // MARK: - Private Methods

    @objc private func confirmPressed() {
        output.didPressConfirm()
    }

    @objc private func cancelPressed() {
        output.didPressCancel()
    }

    private func update(to state: HabitantEditor.ViewState) {
        
        if state.loading {
            navigationItem.leftBarButtonItem?.isEnabled = false
            navigationItem.rightBarButtonItem = loaderItem
        } else {
            navigationItem.leftBarButtonItem?.isEnabled = true
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                title: "Confirm",
                style: .plain,
                target: self,
                action: #selector(confirmPressed)
            )
        }

        guard isViewLoaded else { return }

        nameField.text = state.transformer.name.isEmpty ? nil : state.transformer.name
        nameField.isEnabled = !state.loading

        let selectedSegment: Int
        switch state.transformer.team {
        case .autobots:    selectedSegment = 0
        case .decepticons: selectedSegment = 1
        }

        teamSegmentedControl.selectedSegmentIndex = selectedSegment
        teamSegmentedControl.isEnabled = !state.loading

        characteristicsLabels.forEach {
            label in
            label.text = "\(state.transformer[keyPath: paths[label.tag]].value)"
        }

        characteristicsSteppers.forEach {
            stepper in
            stepper.value = Double(state.transformer[keyPath: paths[stepper.tag]].value)
            stepper.isEnabled = !state.loading
        }

        deleteButton.isHidden = state.transformer.id == nil
        deleteButton.isEnabled = !state.loading
    }
}


