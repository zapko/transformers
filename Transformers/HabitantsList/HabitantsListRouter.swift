//
//  HabitantsListRouter.swift
//  Transformers
//
//  Created by Konstantin Zabelin on 2019-09-06.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import UIKit


protocol HabitantsListRouter {

    func openEditor(
        _ source: Transformer?,
        delegate: HabitantEditorDelegate,
        whenDone: (() -> Void)?
    ) -> HabitantEditorModule

    func closeEditor(whenDone: (() -> Void)?)
    func showMessage(_ message: String, whenDone: (() -> Void)?)
}

extension HabitantsList {
    
    class Router: HabitantsListRouter {


        // MARK: - Private State

        private weak var view: UIViewController?


        // MARK: - Initialization / Deinitialization

        init(view: UIViewController) {
            self.view = view
        }


        // MARK: - HabitantsListRouter

        func openEditor(
            _ source: Transformer?,
            delegate: HabitantEditorDelegate,
            whenDone: (() -> Void)?
        ) -> HabitantEditorModule {

            let factory = HabitantEditor.Factory()
            let (editorView, editorModule) = factory.make(transformer: source, delegate: delegate)

            guard let vc = view else {
                assertionFailure("No VC for navigation manipulation")
                whenDone?()
                return editorModule
            }
            
            editorView.modalPresentationStyle = .pageSheet
            vc.present(editorView, animated: true, completion: whenDone)

            return editorModule
        }

        func closeEditor(whenDone: (() -> Void)?) {

            guard let vc = view else {
                assertionFailure("No VC for navigation manipulation")
                whenDone?()
                return
            }

            guard let presentedView = vc.presentedViewController else {
                assertionFailure("Closing editor, although it's not opened")
                whenDone?()
                return
            }

            presentedView.dismiss(animated: true, completion: whenDone)
        }

        func showMessage(_ message: String, whenDone: (() -> Void)?) {

            guard let vc = view else {
                assertionFailure("No VC for navigation manipulation")
                whenDone?()
                return
            }

            let dismiss = UIAlertAction(title: "Dismiss", style: .cancel)

            let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alertController.addAction(dismiss)

            var presenter = vc
            while let viewOnTop = presenter.presentedViewController {
                presenter = viewOnTop
            }

            presenter.present(alertController, animated: true, completion: whenDone)
        }
    }
}
