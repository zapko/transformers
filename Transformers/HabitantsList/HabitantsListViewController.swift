//
//  HabitantListViewController.swift
//  Transformers
//
//  Created by Konstantin Zabelin on 2019-09-06.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import Foundation
import UIKit


protocol HabitantsListView: AnyObject {
    var state: HabitantsList.ViewState { get set }
}


class HabitantsListViewController: UIViewController,
                                   HabitantsListView,
                                   UICollectionViewDelegate,
                                   UICollectionViewDataSource {

    private enum Constants {
        static let margin: CGFloat = 15
    }


    // MARK: - Private state

    let loadingIndicator = UIActivityIndicatorView(style: .gray)

    let flowLayout: UICollectionViewFlowLayout = {

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = Constants.margin
        layout.minimumLineSpacing      = Constants.margin
        layout.sectionHeadersPinToVisibleBounds = true
        layout.sectionInset = UIEdgeInsets(
            top:    Constants.margin,
            left:   Constants.margin,
            bottom: Constants.margin,
            right:  Constants.margin
        )
        layout.headerReferenceSize = CGSize(width: 60, height: 60)

        return layout
    }()

    let collectionView = UICollectionView(
        frame:                .zero,
        collectionViewLayout: UICollectionViewFlowLayout()
    )
    

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white

        collectionView.collectionViewLayout = flowLayout
        collectionView.dataSource = self
        collectionView.delegate   = self
        collectionView.register(
            UINib(
                nibName: "HabitantsListHeader",
                bundle:  Bundle(for: HabitantsListViewController.self)
            ),
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier:        "HeaderID"
        )
        [CellContent.none, CellContent.habitant]
            .forEach { collectionView.register($0.nib, forCellWithReuseIdentifier: $0.id) }
        collectionView.backgroundColor       = view.backgroundColor
        collectionView.alwaysBounceVertical  = true
        collectionView.autoresizingMask      = [.flexibleHeight, .flexibleWidth]
        collectionView.frame                 = view.bounds
        collectionView.clipsToBounds         = false
        view.addSubview(collectionView)


        loadingIndicator.hidesWhenStopped   = true
        loadingIndicator.layer.cornerRadius = 5
        loadingIndicator.backgroundColor    = .clear
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(loadingIndicator)
        NSLayoutConstraint.activate([
            loadingIndicator.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: Constants.margin),
            loadingIndicator.leftAnchor.constraint(equalTo: view.leftAnchor, constant: Constants.margin),
            loadingIndicator.widthAnchor.constraint(equalToConstant: 20),
            loadingIndicator.heightAnchor.constraint(equalToConstant: 20)
        ])


        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: "War",
            style: .plain,
            target: self,
            action: #selector(warButtonPressed)
        )
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target:              self,
            action:              #selector(addButtonPressed)
        )

        update(to: state, from: state)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        // |-Width-| = |-Margin-|--Item--|-Margin-|--Item--|-Margin-|
        flowLayout.itemSize = CGSize(
            width: (collectionView.frame.width - 3 * Constants.margin) / 2,
            height: 200
        )
    }

    // MARK: - HabitantsListViewController

    var output: HabitantsListViewOutput!


    // MARK: - HabitantsListView

    var state: HabitantsList.ViewState = HabitantsList.ViewState(loading: false, opponents: []) {
        didSet {

            if state == oldValue { return }

            update(to: state, from: oldValue)
        }
    }


    // MARK: - UICollectionViewDelegate

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        guard let habitant = content(at: indexPath, for: state.opponents) else { return }

        output.didPressTransformer(habitant)
    }


    // MARK: - UICollectionViewDataSource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfCells(for: state.opponents)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let habitant = content(at: indexPath, for: state.opponents) else {
            return collectionView.dequeueReusableCell(withReuseIdentifier: CellContent.none.id, for: indexPath)
        }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellContent.habitant.id, for: indexPath)
        (cell as? HabitantsListInfoCell)?.configure(with: habitant)

        return cell
    }

    func collectionView(
        _                                 collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind:           String,
        at                                indexPath:      IndexPath
    ) -> UICollectionReusableView {

        return collectionView.dequeueReusableSupplementaryView(
            ofKind:              kind,
            withReuseIdentifier: "HeaderID",
            for:                 indexPath
        )
    }


    // MARK: - Private Methods

    @objc private func addButtonPressed() {
        output.didPressPlusButton()
    }

    @objc private func warButtonPressed() {
        output.didPressWarButton()
    }

    private func update(to state: HabitantsList.ViewState, from oldState: HabitantsList.ViewState) {

        if state.loading {
            loadingIndicator.startAnimating()
        } else {
            loadingIndicator.stopAnimating()
        }

        collectionView.reloadData()
//        collectionView.performBatchUpdates({
//
////            let oldNumberOfItems = numberOfCells(for: oldState.opponents)
////            let newNumberOfItems = numberOfCells(for: state.opponents)
////
////            self.collectionView.moveItem(at: <#T##IndexPath##Foundation.IndexPath#>, to: <#T##IndexPath##Foundation.IndexPath#>)
////            self.collectionView.insertItems(at: <#T##[IndexPath]##[Foundation.IndexPath]#>)
////
////
//
//            self.collectionView.reloadSections(IndexSet(integer: 0))
//        })
    }
}

private enum CellContent {
    case none
    case habitant

    var id: String {
        switch self {
        case .none:     return "DummyCellID"
        case .habitant: return "InfoCellID"
        }
    }

    var nib: UINib {
        switch self {
        case .none:     return UINib(nibName: "HabitantsListDummyCell", bundle: Bundle(for: HabitantsListViewController.self))
        case .habitant: return UINib(nibName: "HabitantsListInfoCell",  bundle: Bundle(for: HabitantsListViewController.self))
        }
    }
}

private func numberOfCells(for state: [Battle.Pair]) -> Int {
    return state.count * 2
}

/// Autobots will be displayed in the left column and Decepticons in the right one, thus math.
/// Left column should have even indices, right - odd.
private func content(at path: IndexPath, for state: [Battle.Pair]) -> Transformer? {
    assert(path.section == 0)

    let pair             = state[path.row / 2]
    let isLeftColumnPath = path.row % 2 == 0

    return isLeftColumnPath ? pair.autobot : pair.decepticon
}

private extension Battle.Pair {

    var autobot: Transformer? {
        switch self {
        case .onlyAutobot(let a): return a
        case .onlyDecepticon:     return nil
        case .pair(let a, _):     return a
        }
    }

    var decepticon: Transformer? {
        switch self {
        case .onlyAutobot:           return nil
        case .onlyDecepticon(let d): return d
        case .pair(_, let d):        return d
        }
    }
}


