//
//  HabitantsListRouter_Spec.swift
//  Transformers
//
//  Created by Konstantin Zabelin on 2019-09-08.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import XCTest
import UIKit
import Foundation
@testable import Transformers


class HabitantsListRouter_Spec: XCTestCase {

    func test_When_open_editor_is_requested_Router_presents_it_modally() {

        let vc = UIViewController()

        let window = UIWindow()
        window.addSubview(vc.view)

        let subject = HabitantsList.Router(view: vc)

        testAsyncCode("Presentation animation") {
            done in

            let _ = subject.openEditor(nil, delegate: HabitantEditorDelegateMock()) {
                XCTAssertNotNil(vc.presentedViewController)
                done()
            }
        }
    }

    func test_When_close_editor_is_requested_Router_dismisses_what_was_presented_modally() {

        let vc     = UIViewController()
        let editor = UIViewController()

        let window = UIWindow()
        window.addSubview(vc.view)
        
        let subject = HabitantsList.Router(view: vc)

        testAsyncCode("Presentation animation") {
            done in

            vc.present(editor, animated: false) {

                subject.closeEditor {

                    XCTAssertNil(vc.presentedViewController)
                    done()
                }
            }
        }
    }

    func test_When_show_message_is_required_Router_presents_alert_view() {

        let vc = UIViewController()

        let window = UIWindow()
        window.addSubview(vc.view)

        let subject = HabitantsList.Router(view: vc)

        testAsyncCode("Presentation animation") {
            done in

            subject.showMessage("Some message") {
                XCTAssertNotNil(vc.presentedViewController)
                XCTAssertNotNil(vc.presentedViewController as? UIAlertController)
                done()
            }
        }
    }

    func test_When_show_message_is_required_and_editor_is_being_present_Router_presents_alert_view_on_top() {

        let vc     = UIViewController()
        let editor = UIViewController()

        let window = UIWindow()
        window.addSubview(vc.view)

        let subject = HabitantsList.Router(view: vc)

        testAsyncCode("Presentation animation") {
            done in

            vc.present(editor, animated: false) {

                subject.showMessage("Message on top of editor") {

                    XCTAssertNotNil(vc.presentedViewController?.presentedViewController)
                    XCTAssertNotNil(vc.presentedViewController?.presentedViewController as? UIAlertController)
                    done()
                }
            }
        }
    }
}

private class HabitantEditorDelegateMock: HabitantEditorDelegate {

    func editingConfirmed(withResult: Transformer) { }
    func editingDeleted(_ transformer: Transformer) { }
    func editingCancelled() { }
}