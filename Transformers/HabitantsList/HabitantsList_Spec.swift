//
// Created by Konstantin Zabelin on 2019-09-07.
// Copyright (c) 2019 Zababako Studio Inc. All rights reserved.
//

import XCTest
import PromiseKit
@testable import Transformers


class HabitantsList_Spec: XCTestCase {

    private struct Test {
        let subject: HabitantsList.Logic

        let viewMock:   ViewMock
        let routerMock: RouterMock

        let editorMock: EditorMock
        let sparkMock:  AllSparkMock
    }

    func test_On_warm_up_Logic_starts_to_download_habitants() {

        let test = prepareTest()

        testAsyncCode("Waiting for AllSpark to load") {
            done in
            
            test.subject.warmUp()

            after(seconds: 0.01).done {
                XCTAssertGreaterThan(test.sparkMock.t_retrieveTransformersCallCounter, 0)
                done()
            }
        }
    }

    func test_While_data_is_being_loaded_Logic_sets_view_state_to_loading() {

        let test = prepareTest()

        XCTAssertFalse(test.viewMock.state.loading)
        test.subject.warmUp()
        XCTAssertTrue(test.viewMock.state.loading)
    }

    func test_If_data_retrieval_fails_Logic_shows_error_message_and_turn_off_loading() {

        let test = prepareTest()

        test.sparkMock.t_retrieveTransformersResponse = Promise(error: "Some error")

        testAsyncCode("Waiting for AllSpark to load") {
            done in

            test.subject.warmUp()
            XCTAssertTrue(test.viewMock.state.loading)

            after(seconds: 0.01).done {
                XCTAssertFalse(test.viewMock.state.loading)
                XCTAssertEqual(test.sparkMock.t_retrieveTransformersCallCounter, 1)
                XCTAssertEqual(test.routerMock.t_showMessageArguments.count, 1)
                done()
            }
        }
    }

    func test_When_plus_button_is_pressed_Logic_presents_editor() {

        let test = prepareTest()

        test.subject.didPressPlusButton()

        XCTAssertEqual(test.routerMock.t_openEditorSource, [nil] as [Transformer?])
    }

    func test_When_participant_is_selected_Logic_opens_editor_and_passes_it_there() {

        let test = prepareTest()

        test.subject.didPressTransformer(punch)

        XCTAssertEqual(test.routerMock.t_openEditorSource, [punch])
    }

    func test_When_editor_finishes_editing_Logic_sends_new_data_to_spark_and_if_succeeds_refreshes_and_dismisses_editor() {

        let test = prepareTest()

        test.sparkMock.t_inhabitWithTransformerResponse = .value(punch)
        test.sparkMock.t_retrieveTransformersResponse = .value([punch])

        testAsyncCode("Waiting for data to be sent") {
            done in
            
            test.subject.didPressPlusButton()
            test.subject.editingConfirmed(withResult: wildPunch)

            after(seconds: 0.01).done {
                XCTAssertEqual(test.sparkMock.t_inhabitWithTransformerCallArgument, [wildPunch])
                XCTAssertEqual(test.sparkMock.t_retrieveTransformersCallCounter, 1)
                XCTAssertEqual(test.routerMock.t_closeEditorCallCounter, 1)
                XCTAssertEqual(test.viewMock.state.opponents, [Battle.Pair.onlyAutobot(punch)])
                done()
            }
        }
    }

    func test_When_editor_finishes_editing_Logic_sends_new_data_to_spark_and_if_fails_displays_message() {

        let test = prepareTest()
        
        test.sparkMock.t_inhabitWithTransformerResponse = Promise(error: "Some error")
        
        testAsyncCode("Waiting for data to be sent") {
            done in
            
            test.subject.didPressPlusButton()
            test.subject.editingConfirmed(withResult: wildPunch)
            
            after(seconds: 0.01).done {
                XCTAssertEqual(test.sparkMock.t_inhabitWithTransformerCallArgument, [wildPunch])
                XCTAssertEqual(test.routerMock.t_closeEditorCallCounter, 0)
                XCTAssertEqual(test.routerMock.t_showMessageArguments.count, 1)
                done()
            }
        }
    }

    func test_When_editor_cancels_editing_Logic_dismisses_editor_and_does_not_send_anything_to_spark() {

        let test = prepareTest()
        
        testAsyncCode("Waiting for data to be sent") {
            done in
            
            test.subject.didPressPlusButton()
            test.subject.editingCancelled()

            after(seconds: 0.01).done {
                XCTAssertEqual(test.sparkMock.t_inhabitWithTransformerCallArgument, [])
                XCTAssertEqual(test.routerMock.t_closeEditorCallCounter, 1)
                XCTAssertEqual(test.routerMock.t_showMessageArguments.count, 0)
                done()
            }
        }
    }

    func test_When_delete_is_pressed_Logic_deletes_selected_transformer_from_spark_and_if_succeeds_refreshes_and_updates_view() {

        let test = prepareTest()

        test.sparkMock.t_banishTransformerResponse    = .value(())
        test.sparkMock.t_retrieveTransformersResponse = .value([])
        test.viewMock.state.opponents = [.onlyAutobot(punch)]

        testAsyncCode("Waiting for data to be sent") {
            done in

            test.subject.didPressDelete(punch)
            XCTAssertTrue(test.viewMock.state.loading)

            after(seconds: 0.01).done {
                XCTAssertEqual(test.sparkMock.t_banishTransformerCallArgument, [punch.id])
                XCTAssertEqual(test.sparkMock.t_retrieveTransformersCallCounter, 1)
                XCTAssertEqual(test.viewMock.state.opponents, [] as [Battle.Pair])
                XCTAssertFalse(test.viewMock.state.loading)
                done()
            }
        }
    }

    func test_When_delete_button_is_pressed_Logic_deletes_selected_transformer_from_spark_and_if_failure_shows_message() {
        
        let test = prepareTest()
        
        test.sparkMock.t_banishTransformerResponse = Promise(error: "Some error")
        test.viewMock.state.opponents = [.onlyAutobot(punch)]
        
        testAsyncCode("Waiting for data to be sent") {
            done in
            
            test.subject.didPressDelete(punch)
            XCTAssertTrue(test.viewMock.state.loading)
            
            after(seconds: 0.01).done {
                XCTAssertEqual(test.sparkMock.t_banishTransformerCallArgument, [punch.id])
                XCTAssertEqual(test.sparkMock.t_retrieveTransformersCallCounter, 0)
                XCTAssertEqual(test.viewMock.state.opponents, [.onlyAutobot(punch)] as [Battle.Pair])
                XCTAssertFalse(test.viewMock.state.loading)
                XCTAssertEqual(test.routerMock.t_showMessageArguments.count, 1)
                done()
            }
        }
    }

    func test_When_list_is_loaded_Logic_uses_Battle_Rules_to_form_opponents_list_and_set_view_state_accordingly() throws {

        let test = prepareTest()

        let participants = [
            battletrap__D_7__3_6_8__6_6__7_7,
            barrage_____D_3__8_3_7__5_10_9_8,
            seaspray____A_3__8_6_6__6_10_6_7
        ]

        test.sparkMock.t_retrieveTransformersResponse = .value(participants)

        testAsyncCode("Waiting for data to be sent") {
            done in

            test.subject.warmUp()
            XCTAssertTrue(test.viewMock.state.loading)

            after(seconds: 0.01).done {

                let expectedPairs: [Battle.Pair] = [
                    .pair(A: seaspray____A_3__8_6_6__6_10_6_7, D: battletrap__D_7__3_6_8__6_6__7_7),
                    .onlyDecepticon(barrage_____D_3__8_3_7__5_10_9_8)
                ]

                XCTAssertEqual(test.sparkMock.t_retrieveTransformersCallCounter, 1)
                XCTAssertEqual(test.viewMock.state.opponents, expectedPairs)
                XCTAssertFalse(test.viewMock.state.loading)
                done()
            }
        }
    }

    func test_When_war_button_is_pressed_Logic_plays_out_war_remove_perished_from_spark_and_displays_message_with_result() {
        
        let test = prepareTest()

        test.sparkMock.t_retrieveTransformersResponse = .value([
             seaspray____A_3__8_6_6__6_10_6_7,
             ramhorn_____A_8__4_3_9__5_9__3_4,
             searchlight_A_3__7_5_8__6_4__1_9,
             abominus____D_10_1_3_10_5_10_8_4,
             battletrap__D_7__3_6_8__6_6__7_7,
             barrage_____D_3__8_3_7__5_10_9_8
        ])
        test.sparkMock.t_banishTransformerResponse = .value(())

        testAsyncCode("Waiting for data to be sent") {
            done in
            
            test.subject.didPressWarButton()
            XCTAssertTrue(test.viewMock.state.loading)
            
            after(seconds: 0.01).done {

                let expectedPerishedIDs = [
                    abominus____D_10_1_3_10_5_10_8_4.id,
                    ramhorn_____A_8__4_3_9__5_9__3_4.id,
                    searchlight_A_3__7_5_8__6_4__1_9.id
                ]

                XCTAssertEqual(Set(test.sparkMock.t_banishTransformerCallArgument), Set(expectedPerishedIDs))
                XCTAssertEqual(test.sparkMock.t_retrieveTransformersCallCounter, 2)
                XCTAssertEqual(test.routerMock.t_showMessageArguments.count, 1)
                XCTAssertFalse(test.viewMock.state.loading)
                done()
            }
        }
    }


    // MARK: - Preparation part

    private func prepareTest() -> Test {


        let viewMock   = ViewMock()
        let routerMock = RouterMock()
        let editorMock = EditorMock()
        let sparkMock  = AllSparkMock()

        routerMock.t_openEditorResponse = editorMock

        let subject = HabitantsList.Logic(
            view:         viewMock,
            router:       routerMock,
            sparkFactory: { _, _ in .value(sparkMock) }
        )

        return Test(
            subject:    subject,
            viewMock:   viewMock,
            routerMock: routerMock,
            editorMock: editorMock,
            sparkMock:  sparkMock
        )
    }
}

private class ViewMock: HabitantsListView {
    var state: HabitantsList.ViewState = HabitantsList.ViewState(loading: false, opponents: [])
}

private class RouterMock: HabitantsListRouter {

    var t_openEditorSource: [Transformer?] = []
    var t_openEditorResponse: EditorMock!

    func openEditor(
        _ source: Transformer?,
        delegate: HabitantEditorDelegate,
        whenDone: (() -> Void)?
    ) -> HabitantEditorModule {

        t_openEditorSource.append(source)
        XCTAssertNotNil(t_openEditorResponse, "Test was not setup properly")
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(10)) {
            whenDone?()
        }
        return t_openEditorResponse
    }

    var t_closeEditorCallCounter = 0
    func closeEditor(whenDone: (() -> Void)?) {
        t_closeEditorCallCounter += 1
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(10)) {
            whenDone?()
        }
    }

    var t_showMessageArguments: [String] = []
    func showMessage(_ message: String, whenDone: (() -> Void)?) {
        t_showMessageArguments.append(message)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(10)) {
            whenDone?()
        }
    }
}

private class EditorMock: HabitantEditorModule {
    var busy: Bool = false
}

private class AllSparkMock: AllSpark {


    var t_retrieveTransformersCallCounter = 0
    var t_retrieveTransformersResponse: Promise<[Transformer]>?

    func retrieveTransformers() -> Promise<[Transformer]> {

        t_retrieveTransformersCallCounter += 1
        guard let response = t_retrieveTransformersResponse else {
            return Promise(error: "Mock is not preconfigured for the test")
        }
        return response
    }


    var t_inhabitWithTransformerCallArgument: [Transformer] = []
    var t_inhabitWithTransformerResponse:     Promise<Transformer>?

    func inhabit(with transformer: Transformer) -> Promise<Transformer> {

        t_inhabitWithTransformerCallArgument.append(transformer)
        guard let response = t_inhabitWithTransformerResponse else {
            return Promise(error: "Mock is not preconfigured for the test")
        }
        return response
    }


    var t_banishTransformerCallArgument: [Transformer.ID] = []
    var t_banishTransformerResponse:     Promise<Void>?

    func banish(transformer id: Transformer.ID) -> Promise<Void> {

        t_banishTransformerCallArgument.append(id)
        guard let response = t_banishTransformerResponse else {
            return Promise(error: "Mock is not preconfigured for the test")
        }
        return response
    }


    var t_modifyTransformerCallArgument: [Transformer] = []
    var t_modifyTransformerResponse:     Promise<Transformer>?

    func modify(transformer: Transformer) -> Promise<Transformer> {

        t_modifyTransformerCallArgument.append(transformer)
        guard let response =  t_modifyTransformerResponse else {
            return Promise(error: "Mock is not preconfigured for the test")
        }
        return response
    }
}

private let punch = Transformer(
    id:           "123",
    name:         "Punch",
    team:         .autobots,
    rank:         7,
    courage:      10,
    skill:        9,
    strength:     6,
    intelligence: 9,
    speed:        4,
    endurance:    6,
    firepower:    6
)

private let wildPunch = Transformer(
    id:           nil,
    name:         "Punch",
    team:         .autobots,
    rank:         7,
    courage:      10,
    skill:        9,
    strength:     6,
    intelligence: 9,
    speed:        4,
    endurance:    6,
    firepower:    6
)
