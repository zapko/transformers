//
//  HabitantsList+Factory.swift
//  Transformers
//
//  Created by Konstantin Zabelin on 2019-09-06.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit



extension HabitantsList {

    class Factory: MockableModule {

        func make() -> UIViewController {

            let view   = HabitantsListViewController(nibName: nil, bundle: nil)
            let router = HabitantsList.Router(view: view)
            let logic  = HabitantsList.Logic(
                view:         view,
                router:       router,
                sparkFactory: {
                    AllSparkCached
                        .prepareAllSpark($0, api: API(), onStateUpdate: $1)
                        .map { $0 as AllSpark }
            }
            )

            logic.warmUp()

            view.output = logic
            view.title = "Habitants"

            return UINavigationController(rootViewController: view)
        }


        // MARK: - MockableModule

        func mock(state: String) -> UIViewController {

            let view = HabitantsListViewController(nibName: nil, bundle: nil)
            view.title = "Habitants"

            view.state = ViewState(state)

            return UINavigationController(rootViewController: view)
        }
    }
}

private extension HabitantsList.ViewState {
    init(_ state: String) {
        loading = state.lowercased().contains("loading")
        opponents = [
            .pair(A: .searchlight_A_3__7_5_8__6_4__1_9, D: .abominus____D_10_1_3_10_5_10_8_4),
            .pair(A: .seaspray____A_3__8_6_6__6_10_6_7, D: .battletrap__D_7__3_6_8__6_6__7_7),
            .onlyAutobot(try! Transformer(from: "Ramhorn, A, 8, 4, 8, 9, 5, 9, 3, 4")),
        ]

        if state.lowercased().contains("optimus") {
            opponents.insert(.onlyAutobot(try! Transformer(from: "Optimus Prime, A, 8, 10, 8, 9, 6, 9, 10, 8")), at: 0)
        }
    }
}

private class LogicMock: HabitantsListViewOutput {

    func didPressPlusButton() { }
    func didPressTransformer(_ transformer: Transformer) { }
    func didPressDelete(_ transformer: Transformer) { }
    func didPressWarButton() { }
}
