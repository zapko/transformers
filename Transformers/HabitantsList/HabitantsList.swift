//
//  HabitantsList.swift
//  Transformers
//
//  Created by Konstantin Zabelin on 2019-09-06.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import Foundation
import PromiseKit


protocol HabitantsListViewOutput {
    func didPressPlusButton()
    func didPressTransformer(_ transformer: Transformer)
    func didPressDelete(_ transformer: Transformer)
    func didPressWarButton()
}


struct HabitantsList {

    struct ViewState: Equatable {
        var loading:   Bool
        var opponents: [Battle.Pair]
    }

    class Logic: HabitantsListViewOutput,
                 HabitantEditorDelegate {


        // MARK: - Private State

        private weak var view:            HabitantsListView?
        private let      router:          HabitantsListRouter
        private let      allSparkFactory: (Data?, @escaping (Data) -> Void) -> Promise<AllSpark>

        private var allSpark: AllSpark!

        private var editor: HabitantEditorModule?


        // MARK: - Initialization / Deinitialization

        init(
            view:         HabitantsListView,
            router:       HabitantsListRouter,
            sparkFactory: @escaping (Data?, @escaping (Data) -> Void) -> Promise<AllSpark>
        ) {
            self.view            = view
            self.router          = router
            self.allSparkFactory = sparkFactory
        }


        // MARK: - HabitantsList.Logic

        func warmUp() {
            view?.state.loading = true
            allSparkPromise
                .then { $0.retrieveTransformers() }
                .done {
                    [view] in view?.state.opponents = War.formPairs(from: $0)
                }
                .catch {
                    [router] in router.showMessage("Error happened: '\($0)'", whenDone: nil)
                }
                .finally {
                    [view] in view?.state.loading = false
                }
        }


        // MARK: - HabitantsListViewOutput

        func didPressPlusButton() {
            assert(editor == nil)
            editor = router.openEditor(nil, delegate: self, whenDone: nil)
        }

        func didPressTransformer(_ transformer: Transformer) {
            assert(editor == nil)
            editor = router.openEditor(transformer, delegate: self, whenDone: nil)
        }

        func didPressDelete(_ transformer: Transformer) {

            view?.state.loading = true
            allSparkPromise
                .then {
                    spark in
                    spark
                        .banish(transformer: transformer.id)
                        .then { spark.retrieveTransformers() }
                }
                .done {
                    [view] in view?.state.opponents = War.formPairs(from: $0)
                }
                .catch {
                    [router] in router.showMessage("Error happened while deleting: '\($0)'", whenDone: nil)
                }
                .finally {
                    self.view?.state.loading = false
                }
        }
        
        func didPressWarButton() {

            view?.state.loading = true

            allSparkPromise
                .then {
                    [router]
                    spark in
                    spark
                        .retrieveTransformers()
                        .map(War.startWar)
                        .then {
                            warResult -> Promise<Void> in

                            let banishmentPromises = warResult
                                .perished
                                .map { $0.id }
                                .map(spark.banish(transformer:))

                            return when(fulfilled: banishmentPromises)
                                .done { router.showMessage(warResult.message, whenDone: nil) }

                        }
                        .then { spark.retrieveTransformers() }
                }
                .done {
                    [view] in view?.state.opponents = War.formPairs(from: $0)
                }
                .catch {
                    [router] in router.showMessage("Error happened while deleting: '\($0)'", whenDone: nil)
                }
                .finally {
                    self.view?.state.loading = false
                }
        }


        // MARK: - HabitantEditorDelegate

        func editingConfirmed(withResult editee: Transformer) {

            assert(editor != nil)

            editor?.busy = true
            allSparkPromise
                .then {
                    spark -> Promise<[Transformer]> in

                    if editee.id == nil {
                        return spark.inhabit(with: editee)
                                    .then { _ in spark.retrieveTransformers() }

                    } else {
                        return spark.modify(transformer: editee)
                                    .then { _ in spark.retrieveTransformers() }
                    }
                }
                .done {
                    [router, view] habitants in

                    view?.state.opponents = War.formPairs(from: habitants)
                    router.closeEditor { self.editor = nil }
                }
                .catch {
                    [router] in

                    router.showMessage("Error happened: '\($0)'", whenDone: nil)
                    self.editor?.busy = false
                }
        }

        func editingDeleted(_ transformer: Transformer) {

            guard let id = transformer.id else {
                assertionFailure("Trying to delete Transformer that hasn't been saved yet")
                return
            }

            editor?.busy = true
            allSparkPromise
                .then {
                    spark -> Promise<[Transformer]> in

                    spark
                        .banish(transformer: id)
                        .then { spark.retrieveTransformers() }
                }
                .done {
                    [router, view] habitants in

                    view?.state.opponents = War.formPairs(from: habitants)
                    router.closeEditor { self.editor = nil }
                }
                .catch {
                    [router] in

                    router.showMessage("Error happened: '\($0)'", whenDone: nil)
                    self.editor?.busy = false
                }
        }

        func editingCancelled() {

            assert(editor != nil)
            router.closeEditor { self.editor = nil }
        }


        // MARK: - Private Methods

        private var allSparkPromise: Promise<AllSpark> {
            if let spark = allSpark {
                return .value(spark)
            } else {

                return allSparkFactory(loadState(), saveState)
                    .get { self.allSpark = $0 }
            }
        }
    }
}

private extension War.Result {
    var message: String {

        let winnerMessage: String
        switch winner {
        case .none:               winnerMessage = "No winner"
        case .some(.autobots):    winnerMessage = "Autobots won"
        case .some(.decepticons): winnerMessage = "Decepticons won"
        }

        let perishedMessage = (["- Perished - "] + perished.map { $0.name }).joined(separator: "\n")

        let survivedMessage = (["- Survived - "] + survived.map { $0.name }).joined(separator: "\n") 

        return [
            winnerMessage,
            perished.isEmpty ? nil : perishedMessage,
            survived.isEmpty ? nil : survivedMessage
        ].compactMap { $0 }.joined(separator: "\n\n")
    }
}

func loadState() -> Data? {
    return try? Data(contentsOf: documentsDirectory().appendingPathComponent("state.json"))
}

func saveState(_ data: Data) {
    try? data.write(to: documentsDirectory().appendingPathComponent("state.json"))
}

func documentsDirectory() -> URL {
    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
}
