//
//  HabitantsListInfoCell.swift
//  Transformers
//
//  Created by Zapko on 2019-09-09.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import UIKit


class HabitantsListInfoCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet var gradeIndicators: [UIProgressView]!

    override func awakeFromNib() {
        super.awakeFromNib()

        self.layer.cornerRadius = 5
    }

    func configure(with habitant: Transformer) {

        nameLabel.text = habitant.name

        gradeIndicators.forEach {
            indicator in

            let grade = habitant[keyPath: Transformer.characteristicsPaths[indicator.tag]]

            indicator.tintColor = grade.color
            indicator.progress = (Float(grade.value) - 1) / 9
        }

        backgroundColor = habitant.team.cellBackground
    }
}

private extension Team {
    var cellBackground: UIColor {
        switch self {
        case .autobots:    return UIColor(hue: 0.00, saturation: 0.05, brightness: 0.90, alpha: 1.0)
        case .decepticons: return UIColor(hue: 0.71, saturation: 0.05, brightness: 0.90, alpha: 1.0)
        }
    }
}

private extension Grade {
    var color: UIColor {
        switch self.value {
        case let x where x > 7: return UIColor(hue: 0.28, saturation: 1.0, brightness: 0.77, alpha: 1.0)
        case let x where x > 4: return UIColor(hue: 0.13, saturation: 1.0, brightness: 0.92, alpha: 1.0)
        default:                return UIColor(hue: 0.00, saturation: 1.0, brightness: 0.70, alpha: 1.0)
        }
    }
}
