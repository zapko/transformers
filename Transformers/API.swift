//
// Created by Konstantin Zabelin on 2019-09-04.
// Copyright (c) 2019 Zababako Studio Inc. All rights reserved.
//

import Foundation
import PromiseKit
import PMKFoundation


enum APIError: Error {
    case invalidString
    case missingData
}

protocol Endpoint {

    associatedtype Input
    associatedtype Output

    var method: String { get }

    var URLResolver: (URL, Input) -> URL { get }

    var encoder: (Input) throws -> Data?  { get }
    var decoder: (Data?) throws -> Output { get }
}

struct AnyEndpoint<Input, Output>: Endpoint {

    let method: String

    let URLResolver: (URL, Input) -> URL

    let encoder: (Input) throws -> Data?
    let decoder: (Data?) throws -> Output
}

struct TransformersContainer: Codable {
    let transformers: [Transformer]
}

private let baseURL = URL(string: "https://transformers-api.firebaseapp.com")!

protocol ServerAPI {
    func call<T: Endpoint>(endpoint: T, input: T.Input, bearer: String?) -> Promise<T.Output>
}

struct API: ServerAPI, Codable {

    static let requestSpark = AnyEndpoint<Void, Spark>(
        method:      "GET",
        URLResolver: appender("allspark"),
        encoder:     voidEncoder,
        decoder:     stringDecoder
    )

    static let retrieveTransformers = AnyEndpoint<Void, TransformersContainer>(
        method:      "GET",
        URLResolver: appender("transformers"),
        encoder:     voidEncoder,
        decoder:     standardDecoder
    )

    static let postTransformer = AnyEndpoint<Transformer, Transformer>(
        method:      "POST",
        URLResolver: appender("transformers"),
        encoder:     standardEncoder,
        decoder:     standardDecoder
    )

    static let putTransformer = AnyEndpoint<Transformer, Transformer>(
        method:      "PUT",
        URLResolver: appender("transformers"),
        encoder:     standardEncoder,
        decoder:     standardDecoder
    )

    static let deleteTransformer = AnyEndpoint<Transformer.ID, Void>(
        method:      "DELETE",
        URLResolver: appender("transformers") { $0 },
        encoder:     voidEncoder,
        decoder:     voidDecoder
    )


    // MARK: - ServerAPI

    func call<T: Endpoint>(endpoint: T, input: T.Input, bearer: String?) -> Promise<T.Output> {

        do {

            var request = URLRequest(url: endpoint.URLResolver(baseURL, input))

            request.httpMethod =     endpoint.method
            request.httpBody   = try endpoint.encoder(input)

            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            if let token = bearer {
                request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }

            return URLSession.shared.dataTask(.promise, with: request)
                                    .map { try endpoint.decoder($0.data) }
        } catch {
            return .init(error: error)
        }
    }
}



// MARK: - Private functions

private func appender<T>(_ component: String, specifier: ((T) -> String)? = nil) -> ((URL, T) -> URL) {
    return {
        (base: URL, input: T) -> URL in

        if let additionalComponent = specifier?(input) {
            return base.appendingPathComponent(component)
                       .appendingPathComponent(additionalComponent)
        } else {
            return base.appendingPathComponent(component)
        }
    }
}

private func voidEncoder<T>(_ value: T) -> Data? {
    return nil
}

private func voidDecoder(_ possibleData: Data?) throws -> Void {

}

private func stringDecoder(_ possibleData: Data?) throws -> String {

    guard let data = possibleData else {
        throw APIError.missingData
    }

    guard let string = String(data: data, encoding: .utf8) else {
        throw APIError.invalidString
    }

    return string
}

private func standardEncoder<T: Encodable>(_ value: T) throws -> Data? {

    return try JSONEncoder().encode(value)
}

private func standardDecoder<T: Decodable>(_ possibleData: Data?) throws -> T {

    guard let data = possibleData else { throw APIError.missingData }

    return try JSONDecoder().decode(T.self, from: data)
}


