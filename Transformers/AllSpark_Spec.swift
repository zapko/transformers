//
// Created by Zapko on 2019-09-05.
// Copyright (c) 2019 Zababako Studio Inc. All rights reserved.
//

import XCTest
import PromiseKit
@testable import Transformers


extension String: Error {}

private let optimus = Transformer(
    id:           "Brb",
    name:         "Optimus",
    team:         .autobots,
    rank:         10,
    courage:      5,
    skill:        8,
    strength:     9,
    intelligence: 10,
    speed:        7,
    endurance:    3,
    firepower:    8
)

class AllSpark_Spec: XCTestCase {

    func test_AllSparkCached_caches_retrievals_result() {

        let mock = APIMock()
        mock.responses = [.value(TransformersContainer(transformers: [optimus])), .value("spark")]

        testAsyncCode("") {
            done in

            AllSparkCached
                .prepareAllSpark(nil, api: mock, onStateUpdate: { _ in })
                .then {
                    subject in

                    subject
                        .retrieveTransformers()
                        .then { _ in subject.retrieveTransformers() }

                }
                .catch { XCTFail("2 responses are enough for 3 calls and nothing fails (error: '\($0)')") }
                .finally { done() }
        }
    }

    // This test is not relevant, because AllSpark is not saving cache as part of State anymore
//    func test_AllSparkCached_loads_itself_including_cache() {
//
//        let mock = APIMock()
//        mock.responses = [.value(TransformersContainer(transformers: [optimus])), .value("spark")]
//
//        testAsyncCode("") {
//            done in
//
//            AllSparkCached
//                .prepareAllSpark(nil, api: mock, onStateUpdate: { _ in })
//                .then {
//                    allSpark in
//
//                    allSpark
//                        .retrieveTransformers()
//                        .then {
//                            _ -> Promise<AllSparkCached> in
//                            let subjectState = try allSpark.save()
//                            return AllSparkCached.prepareAllSpark(subjectState, api: mock, onStateUpdate: { _ in })
//                        }
//                }
//                .then {
//                    loadedAllSpark in
//                    loadedAllSpark.retrieveTransformers()
//                    // Should use loaded cache and not make another call
//                }
//                .done { XCTAssertEqual($0, [optimus]) }
//                .catch { XCTFail("2 responses are enough for 4 calls and nothing fails (error: '\($0)')") }
//                .finally { done() }
//        }
//    }

    func test_AllSparkCached_throws_known_error_if_cached_data_is_corrupted() {

        let mock = APIMock()
        mock.responses = [.value(TransformersContainer(transformers: [optimus])), .value("spark")]

        testAsyncCode("") {
            done in

            AllSparkCached
                .prepareAllSpark(Data(), api: mock, onStateUpdate: { _ in })
                .done { _ in XCTFail("Spark preparation should fail if cache is corrupted") }
                .catch {

                    guard case .some(CodingError.unreadableAllSparkCache) = $0 as? CodingError else {
                        XCTFail("Unexpected error")
                        return
                    }
                }
                .finally { done() }
        }
    }

    func test_When_banish_method_is_called_AllSpark_sends_request_to_API_with_appropriate_ID() {

        let mock = APIMock()
        mock.responses = [.value(()), .value("spark")]

        testAsyncCode("Request is sent") {
            done in

            AllSparkCached
                .prepareAllSpark(nil, api: mock, onStateUpdate: { _ in })
                .then {
                    allSpark in
                    allSpark.banish(transformer: "123")
                }
                .done {

                    guard let input = mock.t_callInput.last as? String else {
                        XCTFail("Wrong input was passed")
                        return
                    }

                    XCTAssertEqual(input, "123")
                }
                .catch { XCTFail("2 responses are enough for 2 calls and nothing fails (error: '\($0)')") }
                .finally { done() }
        }
    }
}

private class APIMock: ServerAPI {

    var responses: [Promise<Any>] = []
    var t_callEndpoints: [Any] = []
    var t_callInput:     [Any] = []

    func call<T: Endpoint>(endpoint: T, input: T.Input, bearer: String?) -> Promise<T.Output> {

        t_callEndpoints.append(endpoint)
        t_callInput.append(input)

        if responses.isEmpty {
            return Promise(error: "Not enough responses are preset")
        }

        return responses
            .removeLast()
            .compactMap { $0 as? T.Output }
    }
}
