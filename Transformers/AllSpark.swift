//
// Created by Konstantin Zabelin on 2019-09-05.
// Copyright (c) 2019 Zababako Studio Inc. All rights reserved.
//

import Foundation
import PromiseKit


protocol AllSpark {
    func retrieveTransformers()                 -> Promise<[Transformer]>
    func inhabit(with transformer: Transformer) -> Promise<Transformer>
    func banish(transformer id: Transformer.ID) -> Promise<Void>
    func modify(transformer: Transformer)       -> Promise<Transformer>
}

class AllSparkCached: AllSpark {

    private struct State: Codable {
        let spark: Spark
//        let cache: [Transformer]? // Commented out, so that data is refreshed every time app is started
                                    // otherwise, app will be stuck forever in the cached data set
    }

    
    // MARK: - Private state

    private let spark: Spark
    private let api:   ServerAPI

    private let onStateUpdate: (Data) -> Void

    private var cache: [Transformer]? {
        didSet {
            if cache == oldValue { return }

            do {
                let newState = try save()
                onStateUpdate(newState)
            } catch {
                print("Failed to encode state: '\(error)'")
            }
        }
    }
    
    
    // MARK: - Initialization / Deinitialization

    static func prepareAllSpark(
        _ previousData: Data?,
        api:            ServerAPI,
        onStateUpdate:  @escaping (Data) -> Void
    ) -> Promise<AllSparkCached> {

        if let data = previousData {
            do {
                let state = try JSONDecoder().decode(AllSparkCached.State.self, from: data)
                return .value(AllSparkCached(api: api, state: state, onStateUpdate: onStateUpdate))
            } catch {
                return Promise(error: CodingError.unreadableAllSparkCache("\(error)"))
            }
        }

        return api
            .call(endpoint: API.requestSpark, input: (), bearer: nil)
            .map { AllSparkCached(api: api, spark: $0, onStateUpdate: onStateUpdate) }
    }

    private init(
        api:           ServerAPI,
        spark:         Spark,
        onStateUpdate: @escaping (Data) -> Void
    ) {
        self.api           = api
        self.spark         = spark
        self.onStateUpdate = onStateUpdate
    }

    private init(
        api: ServerAPI,
        state: State,
        onStateUpdate: @escaping (Data) -> Void
    ) {
        self.api           = api
        self.spark         = state.spark
        self.cache         = nil//state.cache
        self.onStateUpdate = onStateUpdate
    }


    // MARK: - AllSparkCached

    func save() throws -> Data {

        let state = State(spark: spark)//, cache: cache)
        return try JSONEncoder().encode(state)
    }


    // MARK: - AllSpark

    func retrieveTransformers() -> Promise<[Transformer]> {

        if let cachedTransformers = cache {
            return .value(cachedTransformers)
        }

        return api
            .call(endpoint: API.retrieveTransformers, input: (), bearer: spark)
            .map { $0.transformers }
            .get { self.cache = $0 }
    }

    func inhabit(with transformer: Transformer) -> Promise<Transformer> {

        return api
            .call(endpoint: API.postTransformer, input: transformer, bearer: spark)
            .get { self.cache?.append($0) }
    }

    func banish(transformer id: Transformer.ID) -> Promise<Void> {

        return api
            .call(endpoint: API.deleteTransformer, input: id, bearer: spark)
            .done { self.cache?.removeAll { $0.id == id }}
    }

    func modify(transformer: Transformer) -> Promise<Transformer> {

        return api
            .call(endpoint: API.putTransformer, input: transformer, bearer: spark)
            .get {
                newTransformer in

                guard let index = self.cache?.firstIndex(where: { $0.id == transformer.id }) else {
                    return
                }

                self.cache?.remove(at: index)
                self.cache?.insert(newTransformer, at: index)
            }
    }
}

