//
// Created by Zapko on 2019-09-07.
// Copyright (c) 2019 Zababako Studio Inc. All rights reserved.
//

import XCTest
@testable import Transformers


class Universe_Spec: XCTestCase {


    // MARK: - Forming Pairs

    func test_No_participants_form_empty_list_of_battle_pairs() {

        XCTAssertEqual(
            War.formPairs(from: []),
            [] as [Battle.Pair]
        )
    }
    
    func test_When_only_one_autobot_participates_one_autobot_half_pair_is_formed() throws {
        
        let autobot = seaspray____A_3__8_6_6__6_10_6_7

        XCTAssertEqual(
            War.formPairs(from: [autobot]),
            [Battle.Pair.onlyAutobot(autobot)]
        )
    }

    func test_When_only_one_decepticon_participates_one_decepticon_half_pair_is_formed() throws {
        
        let decepticon = barrage_____D_3__8_3_7__5_10_9_8

        XCTAssertEqual(
            War.formPairs(from: [decepticon]),
            [Battle.Pair.onlyDecepticon(decepticon)]
        )
    }

    func test_When_one_autobot_and_one_decepticon_participate_one_pair_is_formed() throws {

        let autobot    = seaspray____A_3__8_6_6__6_10_6_7
        let decepticon = barrage_____D_3__8_3_7__5_10_9_8

        XCTAssertEqual(
            War.formPairs(from: [autobot, decepticon]),
            [Battle.Pair.pair(A: autobot, D: decepticon)]
        )
    }

    func test_When_two_autobots_and_one_decepticon_participate_the_one_with_higher_rank_is_paired() throws {

        let autobot1   = ramhorn_____A_8__4_3_9__5_9__3_4
        let autobot2   = seaspray____A_3__8_6_6__6_10_6_7
        let decepticon = barrage_____D_3__8_3_7__5_10_9_8

        XCTAssertEqual(
            War.formPairs(from: [autobot1, autobot2, decepticon]),
            [
                Battle.Pair.pair(A: autobot2, D: decepticon),
                Battle.Pair.onlyAutobot(autobot1)
            ]
        )
    }


    // MARK: - Fighting Battles

    func test_When_no_opponent_participant_survives() throws {

        XCTAssertEqual(
            try War.fight(.onlyDecepticon(barrage_____D_3__8_3_7__5_10_9_8)),
            Battle.Result(
                survived: [barrage_____D_3__8_3_7__5_10_9_8],
                perished: []
            )
        )

        XCTAssertEqual(
            try War.fight(.onlyAutobot(searchlight_A_3__7_5_8__6_4__1_9)),
            Battle.Result(
                survived: [searchlight_A_3__7_5_8__6_4__1_9],
                perished: []
            )
        )
    }

    func test_When_one_has_4_or_more_courage_and_3_or_more_strength_difference_above_opponent_it_wins() throws {

        let autobot1    = try Transformer(from: "Au, A, 4, 1, 1, 1, 1, 5, 1, 1")
        let decepticon1 = try Transformer(from: "De, D, 1, 1, 1, 1, 1, 1, 1, 1")

        XCTAssertEqual(
            try War.fight(.pair(A: autobot1, D: decepticon1)),
            Battle.Result(
                survived: [autobot1],
                perished: [decepticon1]
            )
        )

        let autobot2    = try Transformer(from: "Au, A, 1, 1, 1, 1, 1, 1, 1, 1")
        let decepticon2 = try Transformer(from: "De, D, 4, 1, 1, 1, 1, 5, 1, 1")

        XCTAssertEqual(
            try War.fight(.pair(A: autobot2, D: decepticon2)),
            Battle.Result(
                survived: [decepticon2],
                perished: [autobot2]
            )
        )

        let autobot3    = try Transformer(from: "Au, A, 4, 1, 1, 1, 1, 1, 1, 1") // Overall rating is equal
        let decepticon3 = try Transformer(from: "De, D, 1, 4, 1, 1, 1, 5, 1, 1") // Overall rating is equal
        XCTAssertEqual(autobot3.overallRating, decepticon3.overallRating)

        XCTAssertEqual(
            try War.fight(.pair(A: autobot3, D: decepticon3)),
            Battle.Result(
                survived: [],
                perished: [autobot3, decepticon3]
            )
        )
    }
    
    func test_Otherwise_if_skill_difference_is_3_or_more_overall_rating_does_not_help_to_win() throws {

        let autobot1    = try Transformer(from: "Au, A, 9, 9, 9, 9, 1, 1, 9, 1")
        let decepticon1 = try Transformer(from: "De, D, 1, 1, 1, 1, 1, 1, 1, 4")

        XCTAssertEqual(
            try War.fight(.pair(A: autobot1, D: decepticon1)),
            Battle.Result(
                survived: [decepticon1],
                perished: [autobot1]
            )
        )

        let autobot2    = try Transformer(from: "Au, A, 1, 1, 1, 1, 1, 1, 1, 5")
        let decepticon2 = try Transformer(from: "De, D, 9, 9, 9, 9, 1, 1, 9, 1")

        XCTAssertEqual(
            try War.fight(.pair(A: autobot2, D: decepticon2)),
            Battle.Result(
                survived: [autobot2],
                perished: [decepticon2]
            )
        )

        let autobot3    = try Transformer(from: "Au, A, 1, 1, 1, 1, 1, 1, 1, 3")
        let decepticon3 = try Transformer(from: "De, D, 9, 9, 9, 9, 1, 1, 9, 1")

        XCTAssertEqual(
            try War.fight(.pair(A: autobot3, D: decepticon3)),
            Battle.Result(
                survived: [decepticon3],
                perished: [autobot3]
            )
        )
    }
    
    func test_Otherwise_the_one_with_the_largest_overall_rating_wins() throws {
        
        let autobot1    = try Transformer(from: "Au, A, 1, 1, 1, 1, 1, 1, 1, 1")
        let decepticon1 = try Transformer(from: "De, D, 1, 2, 1, 1, 1, 1, 1, 1")
        
        XCTAssertEqual(
            try War.fight(.pair(A: autobot1, D: decepticon1)),
            Battle.Result(
                survived: [decepticon1],
                perished: [autobot1]
            )
        )
        
        let autobot2    = try Transformer(from: "Au, A, 2, 1, 1, 1, 1, 1, 1, 1")
        let decepticon2 = try Transformer(from: "De, D, 1, 1, 1, 1, 1, 1, 1, 1")
        
        XCTAssertEqual(
            try War.fight(.pair(A: autobot2, D: decepticon2)),
            Battle.Result(
                survived: [autobot2],
                perished: [decepticon2]
            )
        )
    }
    
    func test_Optimus_prime_and_Predaking_win_any_fight_regardless() throws {

        let optimus    = try Transformer(from: "Optimus Prime, A, 1, 1, 1, 1, 1, 1, 1, 1")
        let decepticon = try Transformer(from: "De,            D, 9, 9, 9, 9, 9, 9, 9, 9")
        
        XCTAssertEqual(
            try War.fight(.pair(A: optimus, D: decepticon)),
            Battle.Result(
                survived: [optimus],
                perished: [decepticon]
            )
        )
        
        let autobot   = try Transformer(from: "Au,        A, 9, 9, 9, 9, 9, 9, 9, 9")
        let predaking = try Transformer(from: "Predaking, D, 1, 1, 1, 1, 1, 1, 1, 1")
        
        XCTAssertEqual(
            try War.fight(.pair(A: autobot, D: predaking)),
            Battle.Result(
                survived: [predaking],
                perished: [autobot]
            )
        )
    }

    func test_Optimus_prime_and_Predaking_in_one_fight_throw_end_of_the_word_error() throws {

        let optimus   = try Transformer(from: "Optimus Prime, A, 1, 1, 1, 1, 1, 1, 1, 1")
        let predaking = try Transformer(from: "Predaking,     D, 1, 1, 1, 1, 1, 1, 1, 1")

        XCTAssertThrowsError(try War.fight(.pair(A: optimus, D: predaking))) {
            error in

            XCTAssertEqual(Battle.Error.endOfTheWorld, error as? Battle.Error)
        }
    }


    // MARK: - War

    func test_War_with_no_participants_has_no_winner_no_perished_and_no_survivors() {

        let result = War.startWar([])

        XCTAssertNil(result.winner)
        XCTAssertTrue(result.perished.isEmpty)
        XCTAssertTrue(result.survived.isEmpty)
    }

    func test_War_with_one_participant_has_no_winner_no_perished_and_one_survivor() {

        let result = War.startWar([barrage_____D_3__8_3_7__5_10_9_8])

        XCTAssertNil(result.winner)
        XCTAssertEqual(result.survived, [barrage_____D_3__8_3_7__5_10_9_8])
        XCTAssertTrue(result.perished.isEmpty)
    }

    func test_War_with_two_non_equal_opponents_has_a_winner_one_perished_and_one_survivor() {

        let result = War.startWar([
            barrage_____D_3__8_3_7__5_10_9_8,
            searchlight_A_3__7_5_8__6_4__1_9
        ])

        XCTAssertNotNil(result.winner)
        XCTAssertEqual(result.survived.count, 1)
        XCTAssertEqual(result.perished.count, 1)
    }

    func test_War_winner_is_defined_by_number_of_killed_opponents_but_not_by_number_of_survived_teammates() {

        let result2 = War.startWar([
            searchlight_A_3__7_5_8__6_4__1_9,
            seaspray____A_3__8_6_6__6_10_6_7,
            ramhorn_____A_8__4_3_9__5_9__3_4,
            abominus____D_10_1_3_10_5_10_8_4,
        ])

        XCTAssertEqual(result2.winner, Team.decepticons)
        XCTAssertGreaterThan(
            result2.survived.filter { $0.team == .autobots    }.count,
            result2.survived.filter { $0.team == .decepticons }.count
        )
    }

    func test_War_with_Predaking_and_Optimus_in_one_pair_kills_everyone() throws {

        let optimus   = try Transformer(from: "Optimus Prime, A, 1, 1, 1, 1, 10, 1, 1, 1")
        let predaking = try Transformer(from: "Predaking,     D, 1, 1, 1, 1, 10, 1, 1, 1")

        let participants = [
            optimus,
            predaking,
            seaspray____A_3__8_6_6__6_10_6_7,
            ramhorn_____A_8__4_3_9__5_9__3_4,
            searchlight_A_3__7_5_8__6_4__1_9,
            abominus____D_10_1_3_10_5_10_8_4,
            battletrap__D_7__3_6_8__6_6__7_7,
            barrage_____D_3__8_3_7__5_10_9_8
        ]

        let result = War.startWar(participants)

        XCTAssertNil(result.winner)
        XCTAssertTrue(result.survived.isEmpty)
        XCTAssertEqual(result.perished.count, participants.count)
    }
}
