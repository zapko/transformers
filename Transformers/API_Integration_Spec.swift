//
// Created by Zapko on 2019-09-05.
// Copyright (c) 2019 Zababako Studio Inc. All rights reserved.
//

import XCTest
import PromiseKit
@testable import Transformers


class API_Integration_Spec: XCTestCase {

    func test_API_returns_spark() {

        testAsyncCode("Remote call") {
            done in

            api
                .call(endpoint: API.requestSpark, input: (), bearer: nil)
                .done    { spark in XCTAssertFalse(spark.isEmpty) }
                .catch   { XCTFail("Call fails with error: '\($0)'") }
                .finally { done() }
        }
    }

    func test_API_creates_megatron() {

        testAsyncCode("Remote call") {
            done in
            
            retrieveSpark()
                .then { api.call(endpoint: API.postTransformer, input: megatron, bearer: $0) }
                .done {
                    XCTAssertEqual(megatron.name,         $0.name)
                    XCTAssertEqual(megatron.team,         $0.team)
                    XCTAssertEqual(megatron.rank,         $0.rank)
                    XCTAssertEqual(megatron.courage,      $0.courage)
                    XCTAssertEqual(megatron.skill,        $0.skill)
                    XCTAssertEqual(megatron.strength,     $0.strength)
                    XCTAssertEqual(megatron.intelligence, $0.intelligence)
                    XCTAssertEqual(megatron.speed,        $0.speed)
                    XCTAssertEqual(megatron.endurance,    $0.endurance)
                    XCTAssertEqual(megatron.firepower,    $0.firepower)
                }
                .catch   { XCTFail("Call fails with error: '\($0)'") }
                .finally { done() }
        }
    }

    func test_API_updates_transformers() {
        
        testAsyncCode("Remote call") {
            done in
            
            retrieveSpark()
                .then {
                    spark -> Promise<Transformer> in

                    api
                        .call(endpoint: API.postTransformer, input: megatron, bearer: spark)
                        .then {
                            (postedMegatron) -> Promise<Transformer> in

                            var modifiedMegatron = postedMegatron

                            modifiedMegatron.name         = "Megachron"
                            modifiedMegatron.team         = .autobots
                            modifiedMegatron.rank         = 5
                            modifiedMegatron.courage      = 6
                            modifiedMegatron.skill        = 7
                            modifiedMegatron.strength     = 8
                            modifiedMegatron.intelligence = 9
                            modifiedMegatron.speed        = 10
                            modifiedMegatron.endurance    = 9
                            modifiedMegatron.firepower    = 8

                            return api.call(endpoint: API.putTransformer, input: modifiedMegatron, bearer: spark)
                        }
                }
                .done {
                    changedMegatron in

                    XCTAssertEqual(changedMegatron.name,         "Megachron")
                    XCTAssertEqual(changedMegatron.team,         .autobots)
                    XCTAssertEqual(changedMegatron.rank,         5)
                    XCTAssertEqual(changedMegatron.courage,      6)
                    XCTAssertEqual(changedMegatron.skill,        7)
                    XCTAssertEqual(changedMegatron.strength,     8)
                    XCTAssertEqual(changedMegatron.intelligence, 9)
                    XCTAssertEqual(changedMegatron.speed,        10)
                    XCTAssertEqual(changedMegatron.endurance,    9)
                    XCTAssertEqual(changedMegatron.firepower,    8)
                }
                .catch   { XCTFail("Call fails with error: '\($0)'") }
                .finally { done() }
        }
    }

    func test_API_returns_created_transformers() {

        testAsyncCode("Remote call") {
            done in

            retrieveSpark()
                .then {
                    spark in

                    api
                        .call(endpoint: API.postTransformer, input: megatron, bearer: spark)
                        .then {
                            postedMegatron in

                            api
                                .call(endpoint: API.retrieveTransformers, input: (), bearer: spark)
                                .done { XCTAssertEqual($0.transformers, [postedMegatron]) }

                        }
                }
                .catch   { XCTFail("Call fails with error: '\($0)'") }
                .finally { done() }
        }
    }

    func test_API_deletes_created_transformers() {

        testAsyncCode("Remote call") {
            done in

            retrieveSpark()
                .then {
                    spark in

                    api
                        .call(endpoint: API.postTransformer, input: megatron, bearer: spark)
                        .then { api.call(endpoint: API.deleteTransformer, input: $0.id, bearer: spark) }
                        .then { api.call(endpoint: API.retrieveTransformers, input: (), bearer: spark) }
                        .done { XCTAssert($0.transformers.isEmpty) }
                }
                .catch   { XCTFail("Call fails with error: '\($0)'") }
                .finally { done() }
        }
    }


    // MARK: - Private Methods

    private func retrieveSpark() -> Promise<Spark> {
        return api.call(endpoint: API.requestSpark, input: (), bearer: nil)
    }

}

private let api = API()

private let megatron = Transformer(
    id:           nil,
    name:         "Megatron",
    team:         .decepticons,
    rank:         10,
    courage:      9,
    skill:        9,
    strength:     10,
    intelligence: 10,
    speed:        4,
    endurance:    8,
    firepower:    10
)
