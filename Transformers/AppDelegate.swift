//
//  AppDelegate.swift
//  transformers
//
//  Created by Konstantin Zabelin on 2019-09-03.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    // MARK: - UIApplicationDelegate

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let newWindow = UIWindow()
        newWindow.rootViewController = HabitantsList.Factory().make()
        newWindow.makeKeyAndVisible()
        self.window = newWindow

        return true
    }
}
