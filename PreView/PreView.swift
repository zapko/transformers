//
//  PreView.swift
//  Transformers
//
//  Created by Zapko on 2020-01-14.
//  Copyright © 2020 Zababako Studio Inc. All rights reserved.
//

import UIKit


@IBDesignable
class PreView: UIView {

    @IBInspectable
    var module: String = ""

    @IBInspectable
    var state: String = ""
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        let vc = Catalog
            .module(module)
            .mock(state: state)

        addSubview(vc.view)
        vc.view.frame = bounds
        vc.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}

protocol MockableModule {
    func mock(state: String) -> UIViewController
}

enum Catalog {

    static func module(_ moduleName: String) -> MockableModule {

        switch moduleName.lowercased() {

        // Cases can be generated based on conformance to MockableModule
        case "habitantslist":  return HabitantsList.Factory()
        case "habitanteditor": return HabitantEditor.Factory()

        default: fatalError("Unknown module '\(moduleName)'")
        }
    }
}

