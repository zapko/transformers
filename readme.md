
# Transformers
## How to start

0) Checkout repository or open an archive
1) Open `Transformers.xcproject`
2) If you want to run app on device, go to each target settings and change the team to match your local certificates
2) Build and run


## Assumptions

- Transformers that participated in one battle do not participate in the other
  ones. All battles are happening "at the same time", and results of each are
combined into overal war result.
- When combat pairs are prepared they are sorted by rank, but within the same 
rank participants are sorted by id
- Teams are well established and do not change often
- Transformer characteristics set is well established and does not change often

## Decisions

- Not to use team-image-link that comes with transformer after creation in a runtime. There is no value in it, it is a static image associated with static hardcoded data type - Teams enum.
- For the same reason decided to display team images in a section header instead of each cell item - they aren't unique per transformer.

## Neglections

- Ignored the problem of possible synchronization issues. There is no
  attempt to resolve conflicts between local and remote data.
- Localization is not taken care of
- There is a lot of room to improve UI - animation of CRUD operations, transition animation when go into editor, battles animation and war result presentation.


