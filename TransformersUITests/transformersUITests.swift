//
//  transformersUITests.swift
//  transformersUITests
//
//  Created by Konstantin Zabelin on 2019-09-03.
//  Copyright © 2019 Zababako Studio Inc. All rights reserved.
//

import XCTest

class transformersUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    func test_Creating_Optimus_Prime_and_starting_war() {
        
        let app = XCUIApplication()
        
        let habitantsNavigationBar = app.navigationBars["Habitants"]
        habitantsNavigationBar.buttons["Add"].tap()
        
        let scrollViewsQuery = app.scrollViews
        
        let nameField = scrollViewsQuery.otherElements.textFields["Pick a name"]
        nameField.tap()
        nameField.typeText("Optimus Prime")
        app.keyboards.buttons["Done"].tap()
        
        func tap(_ n: Int, element: XCUIElement) {
            for _ in 1...n {
                element.tap()
            }
        }
        
        let element = scrollViewsQuery.children(matching: .other).element
        let strengthIncrement     = element.children(matching: .other).element(boundBy: 2).steppers.buttons["Increment"]
        let intelligenceIncrement = element.children(matching: .other).element(boundBy: 3).steppers.buttons["Increment"]
        let speedIncrement        = element.children(matching: .other).element(boundBy: 4).steppers.buttons["Increment"]
        let enduranceIncrement    = element.children(matching: .other).element(boundBy: 5).steppers.buttons["Increment"]
        let rankIncrement         = element.children(matching: .other).element(boundBy: 6).steppers.buttons["Increment"]
        let courageIncrement      = element.children(matching: .other).element(boundBy: 7).steppers.buttons["Increment"]
        let firepowerIncrement    = element.children(matching: .other).element(boundBy: 8).steppers.buttons["Increment"]
        let skillIncrement        = element.children(matching: .other).element(boundBy: 9).steppers.buttons["Increment"]

        tap(5, element: strengthIncrement)
        tap(5, element: intelligenceIncrement)
        tap(3, element: speedIncrement)
        tap(5, element: enduranceIncrement)
        tap(5, element: rankIncrement)
        tap(5, element: courageIncrement)
        tap(3, element: firepowerIncrement)
        tap(5, element: skillIncrement)
        
        app.navigationBars["Editor"].buttons["Confirm"].tap()
        
        app.collectionViews.children(matching: .cell).element(boundBy: 0).otherElements.containing(.staticText, identifier:"Optimus Prime").element.tap()
        
        let elementsQuery = app.scrollViews.otherElements
        elementsQuery/*@START_MENU_TOKEN@*/.buttons["Autobots"]/*[[".segmentedControls.buttons[\"Autobots\"]",".buttons[\"Autobots\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        elementsQuery.buttons["Delete"].tap()
        
        app.navigationBars["Habitants"].doubleTap() // Waiting until operation is finished and user returned to the list
    }
}
